/*
Need for testing and deploying smart contract to a network
*/
const path = require('path'); //directory bath to inbox.sol
const fs = require('fs')
const solc = require('solc');
const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol')
//unix or windows based system

const source = fs.readFileSync(inboxPath,'utf8');
//read in raw source code from contract

module.exports = solc.compile(source, 1).contracts[':Inbox'];
//returns object -> contracts object - if multiple key,value pairs 
/*
Main stuff: object- bytecode, ABI
*/