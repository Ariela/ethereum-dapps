pragma solidity ^0.4.17; //version of solidity - previous syntax won't break with older versions 

contract ArrayTest {
    uint[] public myArray;
    
    function Test() public {
        myArray.push(1);
        myArray.push(10);
        myArray.push(30);
    }

    function getArrayLength() public view returns (uint) {
        return myArray.length;
    }

    function getMyArray() public view returns (uint[]){
        return myArray;
    }

    function getFirstElement() public view returns (uint){
        return myArray[0];
    }
}