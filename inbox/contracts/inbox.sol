pragma solidity ^0.4.17; //version of solidity - previous syntax won't break with older versions 

contract Inbox{ //same as class
    string public message;
    constructor(string initialMessage) public{
        message = initialMessage;
    }
    
    //  function Inbox(string initialMessage) public{//constructor function called one time when deployed to blockchain
    //    message = initialMessage;
    //}
    
    function setMessage(string newMessage) public{
        message = newMessage;
    }
}
    //   function getMessage() public view returns (string){//function declaration, function name, function type, 
    //     return message;//view returns data 
    // }