/*
Functional test that calls functions in smart contract   
*/
const assert = require('assert')
const ganache = require('ganache-cli')
const Web3 = require('web3')
//always cap constructor/class
const web3 = new Web3(ganache.provider());//changes depending on what provider/network
//const provider = ganache.provider()
/* using constructor to create
 instance of web3 need to setup a
 provider(communication from web3 
    library to network)
    */
const {interface, bytecode} = require('../compile');
let accounts
beforeEach(async()=>{
//get a list of all accounts
accounts = await web3.eth.getAccounts()//returns promise

    //use one of those accounts to deploy the contract 
   inbox =  await new web3.eth.Contract(JSON.parse(interface))//cosntructor Contract
    .deploy({data: bytecode, arguments:['Hi DApp!']})
    .send({from: accounts[0],gas:'1000000'})
    //inbox.setProvider(provider)
});
describe('Inbox', () =>{
    it('deploys a contract',() => {
    assert.ok(inbox.options.address[0]) //checks if value exists - fails if null        
    })
    it('has a defualt message', async () => {
        const message = await inbox.methods.message().call();
        assert.equal(message,'Hi DApp!')
    })
    it('can change the message', async () => {
        await inbox.methods.setMessage('Hi, changed it!').send({from: accounts[0]})
        const message = await inbox.methods.message().call();
        assert.equal(message,'Hi, changed it!')
    })
})