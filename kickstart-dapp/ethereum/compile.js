/*
Need for testing and deploying smart contract to a network
*/
const path = require('path'); //directory bath to inbox.sol
const fs = require('fs-extra')
const solc = require('solc');

const buildPath = path.resolve(__dirname, 'build');
fs.removeSync(buildPath); 
//removes folder and everything inside


const campaignPath = path.resolve(__dirname, 'contracts', 'Campaign.sol');
const source = fs.readFileSync(campaignPath,'utf8');
//get path and read in source code

const output = solc.compile(source,1).contracts;
//compile what we pulled from that source - just contracts - contains campaign and campaign factory 

fs.ensureDirSync(buildPath);
//creates build folder

for(let contract in output){
    fs.outputJsonSync(//write json file to specified folder 
        path.resolve(buildPath, contract.replace(':','') + '.json'),
        output[contract] //object to store in each key named file in build directory  
    )
}





module.exports = solc.compile(source, 1).contracts[':Lottery'];
//returns object -> contracts object - if multiple key,value pairs 
/*
Main stuff: object- bytecode, ABI
*/