pragma solidity ^0.4.17; //version of solidity - previous syntax won't break with older versions 

contract CampaignFactory {
    address[] public deployedCampaigns; //list of deployed Campaigns
    //user create new instance of Campaign, recives min param to pass to the new Campaign
    function createCampaign(uint minimum) public{
        address newCampaign = new Campaign(minimum, msg.sender);//return address of new campaign, pass in user info 
        
        deployedCampaigns.push(newCampaign);
        
        //GOTCHA: when create new contract manager is set to factory not new campaign creater 
    }
    function getDeployedCampaigns() public view returns (address[]){
        return deployedCampaigns; //return list of all campagns created 
    }
}

contract Campaign {
    address public manager; //storage area
    uint public minimumContribution;
   // address[] public approvers;
    struct Request { //can have mulitple request, only need to initialize value types not reference types
        string description;
        uint value;
        address recipient;
        bool complete;
        uint approvalCount; //no votes not counted just approved contributors 
        mapping(address => bool) approvals;
    }
    Request[] public requests; //array with struct type
    mapping(address => bool) public approvers;
    uint public approversCount;
    
    modifier restricted(){
        require(msg.sender == manager);
        _;
    }

    constructor(uint minimum, address creator) public {
        manager = creator;
        minimumContribution = minimum;
    }
    function contribute() public payable {
        require(msg.value > minimumContribution);
       // approvers.push(msg.sender);
        approvers[msg.sender] = true;
        approversCount++; //how many people have joined/contributed to contract 
    }
    function createRequest(string description, uint value, address recipient)
    public restricted{
        //new struct of type request and add it to request array 
        // require(approvers[msg.sender]);
        Request memory newRequest = Request({
            description: description,
            value: value,
            recipient: recipient,
            complete: false,
            approvalCount: 0
        });
        requests.push(newRequest);
    }
    function approveRequest(uint index) public {//pass index of request want to approve
        Request storage request = requests[index];
        require(approvers[msg.sender]);//check person is a donor - if in mapping should get back true 
        require(!request.approvals[msg.sender]);//if person has not voted on this request before
        //add address to hash 
        request.approvals[msg.sender] = true;
        request.approvalCount++;
    }
    function finalizeRequest(uint index) public restricted {
        //adds complete flag to requestor mapping 
        Request storage request = requests[index];
        require(request.approvalCount > (approversCount /2));
        require(!request.complete);
        request.recipient.transfer(request.value);
        request.complete = true;
    }
    function getSummary() public view returns (
      uint, uint, uint, uint, address
      ) {
        return (
          minimumContribution,
          this.balance,
          requests.length,
          approversCount,
          manager
        );
    }

    function getRequestsCount() public view returns (uint) {
        return requests.length;
    }
}