/*
Pre configured instance of factory 
*/

import web3 from './web3';
import CampaignFacroty from './build/CampaignFactory.json'

const instance = new web3.eth.Contract(
    JSON.parse(CampaignFacroty.interface),
    '0x5Fd8c7Ff46cCB286AC30DC9801325001c8B6B444'
)
export default instance;