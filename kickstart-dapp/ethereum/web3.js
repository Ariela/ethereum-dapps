/* 
Configuration around web 3
*/
import Web3 from 'web3';

let web3;

if(typeof window !== 'undefined' && window.web3 !== 'undefined'){
    //we are in the browser
    web3 = new Web3(window.web3.currentProvider); 
} else {
 //we are on the server OR the user is not running metamask
 const provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/67302ad74d5b44dcb472e13f7dd82d66');
 web3 = new Web3(provider)
}
export default web3;
