import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import web3 from './web3';
import lottery from './lottery';

class App extends Component{
  state = { //move to store
    manager: '',
    players: [],
    balance: '',
    value: '',
    message: ''
  }
 async componentDidMount(){
      const manager = await lottery.methods.manager().call();
      const players = await lottery.methods.getPlayers().call();
      const balance = await web3.eth.getBalance(lottery.options.address)
      this.setState({manager,players,balance})
}
onSubmit = async (event) => {
  event.preventDefault();
  const accounts = await web3.eth.getAccounts();
  this.setState({message: 'Waiting on transaction....'})
  await lottery.methods.enter().send({
    from: accounts [0],
      value: web3.utils.toWei(this.state.value,'ether')
  })
  this.setState({message: 'Transaction Complete!'})

};
onClick = async (event) => {
  const accounts = await web3.eth.getAccounts();
  this.setState({message: 'Waiting on winner....'})
  await lottery.methods.pickWinner().send({
    from: accounts [0]
  })
  this.setState({message: 'Winner Picked!'})

};
  render() {
    return (
      <div className="App">
        <h1>Lottery</h1>
        <h2>Manager</h2>
        <p>{this.state.manager}</p>
        <h2>Player</h2>
        <p>{this.state.players}</p>
        <h2>Balance - Ether</h2>
        <p>{web3.utils.fromWei(this.state.balance, 'ether')}</p>
        <form onSubmit={this.onSubmit}>
          <h4>Want to try your luck?</h4>
          <div>
            <label>
              Amount of ether to enter
            </label>
            <input
            value = {this.state.value}
            onChange = {event => this.setState({value:
            event.target.value})}
          />
          </div>
          <button>Enter</button>
        </form>
        <hr/>
        <h4>Pick a winner?</h4>
        <button onClick={this.onClick}>Pick Winner</button>
        <hr/>
        <h1>{this.state.message}</h1>
      </div>
    );
  }
}

export default App;
