import Web3 from 'web3';
const web3 = new Web3(window.web3.currentProvider); //get rinkeby network

export default web3;

