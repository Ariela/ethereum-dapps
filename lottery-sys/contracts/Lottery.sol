pragma solidity ^0.4.17; //version of solidity - previous syntax won't break with older versions 

contract Lottery {
    address public manager;
    address[] public players;//can only access one element at a time
    
    constructor() public {
        //msg global variable avaialbe when call and send a transactoin 
        manager = msg.sender; //no declaration - global variable made avaialbe to us
    }
    function enter()  public payable {
        require(msg.value > .01 ether);  //validation - boolean - function existed if false
        players.push(msg.sender);
    }

    function random() private view returns(uint){ //private because its a helper
        return uint(keccak256(abi.encodePacked(block.difficulty, now, players)));
    }
    function pickWinner() public restriced{
        uint index = random() % players.length;
        players[index].transfer(address(this).balance); //transfter - can access on each address 
        //lastWinner = players[index];
        players = new address[](0); //initialize array
        
    }
    modifier restriced(){  //reduce written code DRY
        require(msg.sender == manager);
        _;
        
    }
        //return list of all players 
    function getPlayers() public view returns (address[]){
        return players;
    }
}
